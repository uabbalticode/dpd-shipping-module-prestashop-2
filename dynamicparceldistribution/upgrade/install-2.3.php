<?php

if (!defined('_PS_VERSION_'))
exit;

function upgrade_module_2_3($object, $install = false)
{
    Db::getInstance()->execute('ALTER TABLE `'._DB_PREFIX_.'dpd_delivery_points` ADD cod TinyInt(1)');
    return true;
}
