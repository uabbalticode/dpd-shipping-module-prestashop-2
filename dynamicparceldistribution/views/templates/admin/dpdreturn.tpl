{*
 * 2015 UAB BaltiCode
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License available
 * through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@balticode.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this module to
 * newer versions in the future.
 *
 *  @author    UAB Balticode Kęstutis Kaleckas
 *  @package   Balticode_DPD
 *  @copyright Copyright (c) 2016 UAB Balticode (http://balticode.com/)
 *  @license   http://www.gnu.org/licenses/gpl-3.0.txt  GPLv3
*}
<div style="display:none" id="dpd_return_popup">
    <form action="?controller=AdminDpdReturn&token={$AdminDpdReturn|escape:'htmlall':'UTF-8'}" id="dpd_return_form" method="POST">
        <div class="shipment_info">
            <input type="text" name="Po_parcel_qty" id="Po_parcel_qty" value="1"
                class="validate-not-negative-number validate-digits"/>
            <label for="Po_parcel_qty">{l s='Number of package' mod='dynamicparceldistribution'}</label>
        </div>
        <input class="close-button" type="button" value="{l s='Close' mod='dynamicparceldistribution'}" onClick="hideReturnWindow()">
        <br />
        <br />
        <input type="submit" name="dpd_return_labels" value="{l s='DPD Return Labels' mod='dynamicparceldistribution'}" />
        <input type="submit" name="dpd_return_collection" value="{l s='DPD Collection Return' mod='dynamicparceldistribution'}" />
        <input type="hidden" name="order" value="{$id_order|escape:'htmlall':'UTF-8'}" />
        <input type="hidden" name="parentToken" value="{$smarty.get.token|escape:'htmlall':'UTF-8'}" />
        <input type="hidden" name="QUERY_STRING" value="{$QUERY_STRING|escape:'htmlall':'UTF-8'}" />
    </form>
</div>