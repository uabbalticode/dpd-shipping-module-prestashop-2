<?php
/**
 * 2015 UAB BaltiCode
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License available
 * through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@balticode.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this module to
 * newer versions in the future.
 *
 *  @author    UAB Balticode Kęstutis Kaleckas
 *  @package   Balticode_DPD
 *  @copyright Copyright (c) 2015 UAB Balticode (http://balticode.com/)
 *  @license   http://www.gnu.org/licenses/gpl-3.0.txt  GPLv3
 */

require_once(_PS_MODULE_DIR_.'dynamicparceldistribution'.DIRECTORY_SEPARATOR.'models'.DIRECTORY_SEPARATOR.'Createdownload.php');
require_once(_PS_MODULE_DIR_.'dynamicparceldistribution'.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'API.php');
require_once(_PS_MODULE_DIR_.'dynamicparceldistribution'.DIRECTORY_SEPARATOR.'dynamicparceldistribution.php');
require_once(_PS_MODULE_DIR_.'dynamicparceldistribution'.DIRECTORY_SEPARATOR.'models'.DIRECTORY_SEPARATOR.'DpdLabelRender.php');

class AdminDpdReturnController extends ModuleAdminController
{
    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();

        $this->initContext();
        $this->sendReturnData();
    }

    /**
     * Function send data to DPD API about carrier pickup
     */
    private function sendReturnData()
    {
        $api = new API();
        $parcels = Tools::getValue('Po_parcel_qty');
        $order_id = Tools::getValue('order');
        $successFlag = false;
        if (Tools::getIsset('dpd_return_labels')) {

            $barcodes = DpdLabelRender::getReturnBarcodes(array($order_id), $parcels);
            if ($barcodes == false) {
                foreach (DpdLabelRender::getErrorMessage() as $messgae) {
                    $this->context->cookie->__set('redirect_errors', Tools::displayError($messgae));
                }
            } else {
                $returnDetails = array(
                        'action' => 'parcel_print',
                        'parcels' => join('|', $barcodes),
                    );
                $api = new API();
                $apiReturn = $api->postData($returnDetails);

                $fileName = 'dpd_'.time().'_return.pdf';
                $pathToFile = DpdLabelRender::saveFileContent($apiReturn, _PS_PDF_DIR_.$fileName);

                if ($pathToFile !== false) {
                    DpdLabelRender::download($fileName, $pathToFile, 'application/pdf');
                } else {
                    return false;
                }
            }
        } elseif (Tools::getIsset('dpd_return_collection')) {
                $successMessages = array();
            for ($i = 1; $i <= $parcels; $i++) {
                $orderObject = new Order((int)$order_id);
                $addressObject = new Address((int)$orderObject->id_address_delivery);

                $returnDetails = array(
                    'username' => Configuration::get(DynamicParcelDistribution::CONST_PREFIX.'SERVICE_USERNAME'),
                    'password' => Configuration::get(DynamicParcelDistribution::CONST_PREFIX.'SERVICE_USERPASS'),
                    'cname' => $addressObject->firstname.' '.$addressObject->lastname, //* pickup name
                    'cstreet' => $addressObject->address1, //* pickup street
                    'ccountry' => country::getIsoById($addressObject->id_country), //* pickup country
                    'cpostal' => $addressObject->postcode, //* pickup postal
                    'ccity' => $addressObject->city, //* pickup city
                    'cphone' => $addressObject->phone, // pickup phone
                    'rname' => Configuration::get(DynamicParcelDistribution::CONST_PREFIX.'PICKUP_ADDRESS_NAME'), //* Delivery name
                    'rname2' => Configuration::get(DynamicParcelDistribution::CONST_PREFIX.'PICKUP_ADDRESS_NAME'),
                    'rstreet' => Configuration::get(DynamicParcelDistribution::CONST_PREFIX.'PICKUP_ADDRESS_STREET'), //* Delivery street
                    'rcountry' => Configuration::get(DynamicParcelDistribution::CONST_PREFIX.'PICKUP_ADDRESS_COUNTRY'), //* Delivery country
                    'rpostal' => Configuration::get(DynamicParcelDistribution::CONST_PREFIX.'PICKUP_ADDRESS_ZIP'), //* Delivery postal
                    'rcity' => Configuration::get(DynamicParcelDistribution::CONST_PREFIX.'PICKUP_ADDRESS_CITY'), //* Delivery city
                    'rphone' => Configuration::get(DynamicParcelDistribution::CONST_PREFIX.'PICKUP_ADDRESS_PHONE'), // Delivery phone
                    'action' => 'cr_import'
                );

                $api = new API();
                $apiReturn = $api->postData($returnDetails);

                $doc = new DOMDocument();
                $doc->loadHTML($apiReturn);
                $divs = $doc->documentElement->getElementsByTagName('div');
                $h1 = $doc->documentElement->getElementsByTagName('h1');
                if ($divs != null) {
                    foreach ($divs as $div) {
                        $string = explode('.', $div->textContent);
                        if (stripos(reset($string), 'err') !== false) { //if not false err is found
                            $string = trim(str_replace(array(reset($string), '.'), '', $div->textContent));
                            $this->context->cookie->__set('redirect_errors', Tools::displayError($string));
                        }
                        if (stripos($apiReturn, 'Reference') !== false) {
                            preg_match('/Reference=(.*?)=/i', $apiReturn, $match);
                            $reference = array_reverse($match);
                            $successMessages[] = $this->l('Reference: ').reset($reference).' '.ltrim($apiReturn, reset($match));
                            $successFlag = true;
                        } else {
                            $this->context->cookie->__set('redirect_errors', Tools::displayError($div->textContent));
                        }
                    }
                } else {
                    $this->context->cookie->__set('redirect_errors', Tools::displayError($apiReturn));
                }
            }
        } else {
            $type = false;
        }

        if ($successFlag) {
            $messgae = implode('<br>', $successMessages);
            $this->context->cookie->__set('redirect_success', Tools::displayError($messgae));
        }

        Tools::redirectAdmin('?'.Tools::getValue('QUERY_STRING'));

    }

    /* Retrocompatibility 1.4/1.5 */
    private function initContext()
    {
        if (class_exists('Context')) {
            $this->context = Context::getContext();
        }
        // else
        // {
        //     global $smarty, $cookie;
        //     $this->context = new StdClass();
        //     $this->context->smarty = $smarty;
        //     $this->context->cookie = $cookie;
        // }
    }
}
